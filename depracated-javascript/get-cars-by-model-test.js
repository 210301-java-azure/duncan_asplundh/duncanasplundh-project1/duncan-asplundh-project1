document.getElementById("form-input").addEventListener("submit", (event) => {
    event.preventDefault();
    console.log(selector);

    var loader = document.getElementById("loading-ring");
    loader.hidden = false;
    console.log('event submitted');

    //console.log(selector);
    if (selector == "getAll") {

        return (removeAllChildNodes(tableDataParent), getAllCars());
    }

    if (selector == "getCarByName") {
        removeAllChildNodes(tableDataParent);
        getCarByName();
    }

    if (selector == "updateModel") {
        removeAllChildNodes(tableDataParent);
        updateCarByName();
    }

    if (selector == "deleteModel") {
        deleteCarByName();
    }
});

// function getCarByName() {
//     const model = document.getElementById("get-model-name").value;
//     console.log("car model = " + model);

//     const xhr = new XMLHttpRequest(); // ready state 0 

//     xhr.open("POST", `http://localhost:7000/carmodels/model`);
//     xhr.onreadystatechange = function () {
//         if (xhr.readyState == 4) {
//             if (xhr.status == 200) {
//                 const car = JSON.parse(xhr.responseText);
//                 renderSingleCarInTable(car);
//                 console.log(car);
//                 var carImage = document.getElementById("car-picture");
//                 carImage.src = `${model}.jpg`;
//                 var loader = document.getElementById("loading-ring");
//                 loader.hidden = true;
//             } else {
//                 console.log("something went wrong with your request")
//             }
//         }
//     }
//     xhr.send(`model=${model}`);
