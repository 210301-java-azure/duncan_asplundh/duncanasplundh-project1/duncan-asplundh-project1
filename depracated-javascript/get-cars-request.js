const tableDataParent = document.getElementById("table-data-parent");
document.getElementById("get-cars").addEventListener("click", function () {
    var loader = document.getElementById("loading-ring");
    loader.hidden = false;
    return (removeAllChildNodes(tableDataParent), getAllCars());
});

function getAllCars() {
    const xhr = new XMLHttpRequest(); // ready state 0 
    xhr.open("GET", "http://localhost:7000/carmodels"); // ready state 1
    // let token = sessionStorage.getItem("jwt"); // we could set the Auth header with this value instead
    //xhr.setRequestHeader("Authorization", "admin-auth-token");
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                const cars = JSON.parse(xhr.responseText);
                var carImage = document.getElementById("car-picture");
                carImage.src = "all-cars.jpg"
                var loader = document.getElementById("loading-ring");
                loader.hidden = true;
                renderItemsInTable(cars);
            } else {
                console.log("something went wrong with your request")
            }
        }
    }
    xhr.send(); // ready state 2,3,4 follow 
}
function removeAllChildNodes(parent, e) {
    e.preventDefault();
    while (parent.firstChild) {
        parent.removeChild(parent.firstChild);
    }
}


function renderSingleCarInTable(car) {
    const tableBody = document.getElementById("table-data-parent");
    let newRow = document.createElement("tr");
    newRow.innerHTML = `<td>${car.region}</td><td>${car.make}</td><td>${car.model}</td>
        <td>${car.engine}</td><td>${car.topSpeed}mph</td><td>${car.zeroToSixty}s</td><td>${car.mpg}mpg</td>
        <td>$${car.price}</td>`;
    tableBody.appendChild(newRow);
}



function renderItemsInTable(carsList) {
    const tableBody = document.getElementById("table-data-parent");
    for (let car of carsList) {
        let newRow = document.createElement("tr");
        newRow.innerHTML = `<td>${car.region}</td><td>${car.make}</td><td>${car.model}</td>
        <td>${car.engine}</td><td>${car.topSpeed}mph</td><td>${car.zeroToSixty}s</td><td>${car.mpg}mpg</td>
        <td>$${car.price}</td>`;
        tableBody.appendChild(newRow);
    }
}
