function deleteCarByName() {
    const model = document.getElementById("get-model-name").value;
    const xhr = new XMLHttpRequest(); // ready state 0 
    console.log(`model name for delete is: ${model}`);

    xhr.open("DELETE", `http://localhost:7000/carmodels/`);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                console.log("Car Deleted");
            } else {
                console.log("something went wrong with your request")
            }
        }
    }
    xhr.send(`carName=${model}`);
}