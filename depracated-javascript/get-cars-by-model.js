function getCarByName() {
    const model = document.getElementById("get-model-name").value;
    const xhr = new XMLHttpRequest();

    xhr.open("POST", `http://localhost:7000/carmodels/model`);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                const car = JSON.parse(xhr.responseText);
                // put error message here/html to let user know model doesn't exist
                renderSingleCarInTable(car);
                console.log(car);
                var loader = document.getElementById("loading-ring");
                loader.hidden = true;
            } else {
                console.log("something went wrong with your request")
            }
        }
    }
    xhr.send(`model=${model}`);

    // This is an attempt to call an external API

    // const ExtCarAPI = new XMLHttpRequest();
    // ExtCarAPI.open("POST", `https://api.fuelapi.com/v1/json/vehicles/?year=2011&model=cooper&make=mini&api_key=daefd14b-9f2b-4968-9e4d-9d4bb4af01d1`);
    // ExtCarAPI.onreadystatechange = function () {
    //     if (ExtCarAPI == 4) {
    //         if (ExtCarAPI == 200) {
    //             var carId = JSON.parse(ExtCarAPI.responseText);
    //             console.log(carId);
    //         }
    //     }
    // }
    // ExtCarAPI.send("year=2011&model=cooper&make=mini&api_key=daefd14b-9f2b-4968-9e4d-9d4bb4af01d1");


}