function updateCarByName() {
    const oldModel = document.getElementById("replace-model").value;
    const newModel = document.getElementById("updated-model").value;
    const xhr = new XMLHttpRequest(); // ready state 0 
    console.log(`oldModel is ${oldModel}, new model is ${newModel}`);

    xhr.open("PUT", `http://localhost:7000/carmodels/`);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                const car = JSON.parse(xhr.responseText);
                renderSingleCarInTable(car);
                console.log(car);
                var loader = document.getElementById("loading-ring");
                loader.hidden = true;
            } else {
                console.log("something went wrong with your request")
            }
        }
    }
    xhr.send(`oldModel=${oldModel}&newModel=${newModel}`);
}