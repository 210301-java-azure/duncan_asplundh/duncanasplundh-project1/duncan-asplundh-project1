# Duncan-Asplundh-Project1

[Link To Login Page](http://drasplundhblob.blob.core.windows.net/drasplunhdbloblstorage/login-page.html)
- There are two users available
- `duncan` with password `password`
- or `bob` with password `mypassword`

### NOTE
> Service for this program has been stopped!

# Known Issues:
- No signup function
- delete make is not functional right now
- most fields must be filled out exactly as `model` names appear in database, and is case sensitive.
    - I.E. to seach for a lamborgini diablo, you would have to type in `Diablo` into the search bar
- most fields can only be manipulated via the vehicle's `model` field, this can cause unwanted results
    - I.E. if you have two cars with the same `model` name and attempt to delete/update `model`, all instances with that `model` name will be deleted/updated

# Features
- Get all cars in data base
- Get a specific car by model name
    - will also display an image of said car
- Add a car to the database
- Update a car in the database
- delete a car in the database

# Security
- encrypted JWT are used for user authentication

# Getting Started
- first you'll need to head over to this link [Server Repo](https://gitlab.com/210301-java-azure/duncan_asplundh/duncan-asplundh-project0)
- create/navigate to a directory you wish to store the repo, then use the command `git clone https://gitlab.com/210301-java-azure/duncan_asplundh/duncan-asplundh-project0` to grab the repository
- then navigate to that directory, and find the `Driver` file.
- use the command `javac Driver.java`
- finally use the command `java Driver`
- the server should now be running locally.
- next, you can clone this repository
- use the command `git clone https://gitlab.com/210301-java-azure/duncan_asplundh/duncanasplundh-project1/duncan-asplundh-project1.git`
- navigate to the `login-page.hmtl` file and open it with your favorite browser
- You should now be able to login using one of the two users mentioned above.

