
// Method for determining which crud method we're calling
document.getElementById("form-input").addEventListener("submit", (event) => {
    event.preventDefault();
    console.log(selector);

    var loader = document.getElementById("loading-ring");
    loader.hidden = false;
    console.log('event submitted');

    //console.log(selector);
    if (selector == "getAll") {

        return (removeAllChildNodes(tableDataParent), getAllCars());
    }

    if (selector == "getCarByName") {
        removeAllChildNodes(tableDataParent);
        getCarByName();
    }

    if (selector == "updateModel") {
        removeAllChildNodes(tableDataParent);
        updateCarByName();
    }

    if (selector == "deleteModel") {
        deleteCarByName();
    }

    if (selector == "createMake") {
        addMake();
    }

    if (selector == "createModel") {
        addModel();
    }

    if (selector == "deleteMake") {
        deleteMake();
    }
});