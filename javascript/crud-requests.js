const tableDataParent = document.getElementById("table-data-parent");

// Get All Cars Method
document.getElementById("get-cars").addEventListener("click", function () {
    var loader = document.getElementById("loading-ring");
    loader.hidden = false;                                                  // turn on loading ring
    return (removeAllChildNodes(tableDataParent), getAllCars());
});

function getAllCars() {
    const xhr = new XMLHttpRequest(); // ready state 0 
    xhr.open("GET", "http://localhost:7000/carmodels"); // ready state 1
    let token = sessionStorage.getItem("jwt");
    let username = sessionStorage.getItem("username");
    xhr.setRequestHeader("Authorization", token);
    xhr.setRequestHeader("Username", username);
    console.log("user is: " + username);
    console.log("authorization is: " + token);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                const cars = JSON.parse(xhr.responseText);
                var carImage = document.getElementById("car-picture");
                carImage.src = "all-cars.jpg"
                var loader = document.getElementById("loading-ring");       // Turn off loading ring when successful
                loader.hidden = true;
                renderItemsInTable(cars);
            } else {
                console.log("something went wrong with your request")
            }
        }
    }
    xhr.send();
}

// Get Car By Name Request
function getCarByName() {
    const model = document.getElementById("get-model-name").value;
    const xhr = new XMLHttpRequest();

    xhr.open("POST", `http://localhost:7000/carmodels/model`);
    let token = sessionStorage.getItem("jwt");
    let username = sessionStorage.getItem("username");
    xhr.setRequestHeader("Authorization", token);
    xhr.setRequestHeader("Username", username);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                const car = JSON.parse(xhr.responseText);
                // put error message here/html to let user know model doesn't exist
                renderSingleCarInTable(car);
                console.log(car);
                var loader = document.getElementById("loading-ring");
                loader.hidden = true;
                var carImage = document.getElementById("car-picture");
                carImage.src = `${model}.jpg`;
            } else {
                console.log("something went wrong with your request")
            }
        }
    }
    xhr.send(`carName=${model}`);
}


// Update Car Request
function updateCarByName() {
    const oldModel = document.getElementById("replace-model").value;
    const newModel = document.getElementById("updated-model").value;
    const xhr = new XMLHttpRequest(); // ready state 0 
    console.log(`oldModel is ${oldModel}, new model is ${newModel}`);

    xhr.open("PUT", `http://localhost:7000/carmodels/`);
    let token = sessionStorage.getItem("jwt");
    let username = sessionStorage.getItem("username");
    xhr.setRequestHeader("Authorization", token);
    xhr.setRequestHeader("Username", username);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                const car = JSON.parse(xhr.responseText);
                renderSingleCarInTable(car);
                console.log(car);
                var loader = document.getElementById("loading-ring");
                loader.hidden = true;
            } else {
                console.log("something went wrong with your request")
            }
        }
    }
    xhr.send(`oldName=${oldModel}&newName=${newModel}`);
}

// Delete Car by Name request
function deleteCarByName() {
    const model = document.getElementById("get-model-name").value;
    const xhr = new XMLHttpRequest(); // ready state 0 
    console.log(`model name for delete is: ${model}`);

    xhr.open("DELETE", `http://localhost:7000/carmodels/`);
    let token = sessionStorage.getItem("jwt");
    let username = sessionStorage.getItem("username");
    xhr.setRequestHeader("Authorization", token);
    xhr.setRequestHeader("Username", username);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                console.log("Car Deleted");
            } else {
                console.log("something went wrong with your request")
            }
        }
    }
    xhr.send(`carName=${model}`);
}


function addModel() {

    const xhr = new XMLHttpRequest(); // ready state 0 

    // grab all values for new car
    const model = document.getElementById("get-model").value;
    const engine = document.getElementById("get-engine").value;
    const topSpeed = document.getElementById("get-top-speed").value;
    const zeroToSixty = document.getElementById("get-zero-to-sixty").value;
    const mpg = document.getElementById("get-mpg").value;
    const price = document.getElementById("get-price").value;
    const make = document.getElementById("make-drop-down"); // returns a JSON

    // This was necessary because I needed to store a JSON into the value of an <option> tag, in case you ever need to use
    // the makeId, name, or region from that dropdown menu
    makeObj = make.value;
    makeObj = JSON.parse(makeObj)

    //create a carmodel JSON
    var carModel = { engine: `${engine}`, makeId: `${makeObj.makeId}`, mpg: `${mpg}`, name: `${model}`, price: `${price}`, topSpeed: `${topSpeed}`, zeroToSixty: `${zeroToSixty}` };
    var insertCar = JSON.stringify(carModel);
    console.log("your fancy JSON is: " + insertCar);


    xhr.open("POST", `http://localhost:7000/carmodels/`);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                console.log("car added");
            } else {
                console.log("something went wrong with your request")
            }
        }
    }
    xhr.send(insertCar);
}


// Get All Makes
document.getElementById("get-makes").addEventListener("click", function () {
    var loader = document.getElementById("loading-ring");
    loader.hidden = false;                                                  // turn on loading ring
    return (removeAllChildNodes(tableDataParent), getAllMakes());
});

function getAllMakes() {
    const xhr = new XMLHttpRequest();
    xhr.open("GET", "http://localhost:7000/make");
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                const makes = JSON.parse(xhr.responseText);
                renderItemsInMakeTable(makes);
                var loader = document.getElementById("loading-ring");
                loader.hidden = true;
            } else {
                console.log("something went wrong with your request")
            }
        }
    }
    xhr.send();
}

function addMake() {
    const make = document.getElementById("get-make-name").value;
    const region = document.getElementById("get-make-region").value;
    var makeObj = { name: `${make}`, region: `${region}` };
    var makeJSON = JSON.stringify(makeObj);
    console.log(makeJSON);
    const xhr = new XMLHttpRequest(); // ready state 0 

    xhr.open("POST", `http://localhost:7000/make/`);
    let token = sessionStorage.getItem("jwt");
    let username = sessionStorage.getItem("username");
    xhr.setRequestHeader("Authorization", token);
    xhr.setRequestHeader("Username", username);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                console.log("Make Added");
            } else {
                console.log("something went wrong with your request")
            }
        }
    }
    xhr.send(makeJSON);
}





// Get All Makes and render in dropdown
function getAllMakesForDropDown(callback) {
    const xhr = new XMLHttpRequest();
    xhr.open("GET", "http://localhost:7000/make");
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                const makes = JSON.parse(xhr.responseText);
                //console.log(makes);
                renderMakesInDropDown(makes);
                var loader = document.getElementById("loading-ring");
                loader.hidden = true;
                callback();
            } else {
                console.log("something went wrong with your request")
            }
        }
    }
    xhr.send();

    //return true;
}

function deleteMake() {
    const make = document.getElementById("get-make-name").value;
    const xhr = new XMLHttpRequest();
    console.log(`model name for delete is: ${make}`);

    xhr.open("DELETE", `http://localhost:7000/make/`);
    let token = sessionStorage.getItem("jwt");
    let username = sessionStorage.getItem("username");
    xhr.setRequestHeader("Authorization", token);
    xhr.setRequestHeader("Username", username);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                console.log("Make Deleted");
            } else {
                console.log("something went wrong with your request")
            }
        }
    }
    xhr.send(`makeName=${make}`);
}