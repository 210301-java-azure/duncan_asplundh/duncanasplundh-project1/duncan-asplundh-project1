
// Grab data from form with id="login-form"
document.getElementById("login-form").addEventListener("submit", attemptLogin);

//attempt login function 
function attemptLogin(event) {
    event.preventDefault(); // this stops the form from sending a GET request by default to the same URL
    const username = document.getElementById("inputUsername").value; // this grabs the input from our form field "inputUsername"
    const password = document.getElementById("inputPassword").value; // same as above except grabs Password
    ajaxLogin(username, password, successfulLogin, loginFailed)
}

function successfulLogin(xhr) {
    const authToken = xhr.getResponseHeader("Authorization");
    const user = xhr.responseText;
    sessionStorage.setItem("jwt", authToken);
    sessionStorage.setItem("username", user);
    window.location.href = "./homepage-loggedin.html";
}

function loginFailed(xhr) {
    const errorDiv = document.getElementById("warning");
    const welcomeDiv = document.getElementById("welcome");
    console.log(xhr.responseText);
    errorDiv.style.visibility = "visible";
    errorDiv.innerText = xhr.responseText;
    welcomeDiv.style.visibility = "hidden";
}



function unfade(element) {
    var opacity = 0.1;
    var timer = setInterval(function () {
        if (opacity >= 1) {
            clearInterval(timer);
        }
        element.style.opacity = opacity;
        element.style.filter = 'alpha(opacity=' + opacity * 100 + ")";
        opacity += opacity * 0.1;
    }, 40);
}


setTimeout(function () {
    unfade(document.getElementById("img-container"));
}, 750);

setTimeout(function () {
    unfade(document.getElementById("welcome"));
}, 2000);


/*
const xhr = new XMLHttpRequest();
xhr.open("POST", "http://localhost:7000/login");
xhr.onreadystatechange = function () {
    if (xhr.readyState == 4) {
        if (xhr.status == 200) {
            const credentials = JSON.parse(xhr.responseText);
            console.log(credentials);
        } else {
            console.log("Something went wrong during the request");
        }

    }
}
xhr.send(); //xhr.send(body parameters go here)
*/

/*
function renderItemsInTable(itemsList) {
    const table = document.getElementById("items-table");
    for (let item of itemsList) {
        let newRow = document.createElement("tr");
        newRow.innerHTML = `<td>${item.id}</td><td>${item.name}</td><td>${item.price}</td>`;
        table.appendChild(newRow);
    }
}
*/