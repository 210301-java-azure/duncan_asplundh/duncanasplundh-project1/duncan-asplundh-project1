const formParent = document.getElementById("form-content-parent");
let selector; // selects the appropriate crude method

document.getElementById("get-car-by-model").addEventListener("click", function () {
    selector = "getCarByName";
    return (removeAllChildNodes(formParent), populateGetCarByNameField(), selector);
});

document.getElementById("delete-car").addEventListener("click", function () {
    selector = "deleteModel";
    return (removeAllChildNodes(formParent), populateDeleteCarField());
});

document.getElementById("update-car").addEventListener("click", function () {
    selector = "updateModel";
    return (removeAllChildNodes(formParent), populateUpdateCarField(), selector);
});

document.getElementById("make-entry").addEventListener("click", function () {
    selector = "createModel";
    return (removeAllChildNodes(formParent), populateMakeCarField());
});

document.getElementById("add-make").addEventListener("click", function () {
    selector = "createMake";
    return (removeAllChildNodes(formParent), populateAddMakeField())
});

document.getElementById("delete-make").addEventListener("click", function () {
    selector = "deleteMake";
    return (removeAllChildNodes(formParent), populateDeleteMakeField())
});



function populateUpdateCarField() {
    let inputUpdate = document.createElement("input");
    inputUpdate.placeholder = "With Model";
    inputUpdate.id = "updated-model";
    let inputReplace = document.createElement("input");
    inputReplace.placeholder = "Replace Model";
    inputReplace.id = "replace-model";
    let updateButton = document.createElement("input");
    // updateButton.class = "btn btn-dark";
    var buttonAtt = document.createAttribute("class");
    buttonAtt.value = "btn btn-dark";
    updateButton.setAttributeNode(buttonAtt);
    updateButton.innerHTML = "Update";
    updateButton.id = "update-car";
    updateButton.type = "submit";

    formParent.appendChild(inputReplace);
    formParent.appendChild(inputUpdate);
    formParent.appendChild(updateButton);
}

function populateDeleteCarField() {
    let inputField = document.createElement("input");
    var att = document.createAttribute("placeholder");
    att.value = "Model Name";
    inputField.setAttributeNode(att);
    att = document.createAttribute("id");
    att.value = "get-model-name";
    inputField.setAttributeNode(att);

    let deleteButton = document.createElement("input");
    deleteButton.value = "Delete";
    var buttonAtt = document.createAttribute("class");
    buttonAtt.value = "btn btn-dark";
    deleteButton.setAttributeNode(buttonAtt);
    deleteButton.id = "delete-car";
    deleteButton.type = "submit";
    // buttonAtt = document.createAttribute("id");
    // buttonAtt.value = "delete-car";
    // deleteButton.setAttributeNode(buttonAtt);

    formParent.appendChild(inputField);
    formParent.appendChild(deleteButton);
}


function removeAllChildNodes(parent) {
    while (parent.firstChild) {
        parent.removeChild(parent.firstChild);
    }
}

function populateGetCarByNameField() {
    let inputField = document.createElement("input");
    var att = document.createAttribute("placeholder");
    att.value = "Model Name";
    inputField.setAttributeNode(att);
    att = document.createAttribute("id");
    att.value = "get-model-name";
    inputField.setAttributeNode(att);

    let submitButton = document.createElement("input");
    submitButton.innerHTML = 'Submit';
    var buttonAtt = document.createAttribute("class");
    buttonAtt.value = "btn btn-dark";
    submitButton.setAttributeNode(buttonAtt);
    // submitButton.class = "btn btn-dark";
    submitButton.id = "submit-car";
    submitButton.type = "submit";


    formParent.appendChild(inputField);
    formParent.appendChild(submitButton);
}

function populateAddMakeField() {
    let inputField = document.createElement("input");
    inputField.placeholder = "Make Name";
    inputField.id = "get-make-name";

    let regionField = document.createElement("input");
    regionField.placeholder = "Region";
    regionField.id = "get-make-region";

    let submitButton = document.createElement("input");
    var buttonAtt = document.createAttribute("class");
    buttonAtt.value = "btn btn-dark";
    submitButton.setAttributeNode(buttonAtt);
    submitButton.id = "submit-make";
    submitButton.type = "submit";

    formParent.appendChild(inputField);
    formParent.appendChild(regionField);
    formParent.appendChild(submitButton);

}

function populateDeleteMakeField() {
    let inputField = document.createElement("input");
    inputField.placeholder = "Make Name";
    inputField.id = "get-make-name";

    let submitButton = document.createElement("input");
    var buttonAtt = document.createAttribute("class");
    buttonAtt.value = "btn btn-dark";
    submitButton.setAttributeNode(buttonAtt);
    submitButton.id = "submit-make";
    submitButton.type = "submit";

    formParent.appendChild(inputField);
    formParent.appendChild(submitButton);

}




function populateMakeCarField() {


    getAllMakesForDropDown(function () {
    });

    let model = document.createElement("input");
    model.id = "get-model";
    model.type = "text";
    model.placeholder = "Model";
    let engine = document.createElement("input");
    engine.id = "get-engine";
    engine.type = "text";
    engine.placeholder = "Engine";
    let topSpeed = document.createElement("input");
    topSpeed.id = "get-top-speed";
    topSpeed.type = "text";
    topSpeed.placeholder = "Top Speed";
    let zeroToSixty = document.createElement("input");
    zeroToSixty.id = "get-zero-to-sixty";
    zeroToSixty.type = "text";
    zeroToSixty.placeholder = "0-60";
    let mpg = document.createElement("input");
    mpg.id = "get-mpg";
    mpg.type = "text";
    mpg.placeholder = "MPG";
    let price = document.createElement("input");
    price.id = "get-price";
    price.type = "text";
    price.placeholder = "Price";
    let submitButton = document.createElement("input");
    submitButton.innerHTML = 'Submit';
    var buttonAtt = document.createAttribute("class");
    buttonAtt.value = "btn btn-dark";
    submitButton.setAttributeNode(buttonAtt);
    submitButton.id = "submit-car";
    submitButton.type = "submit";

    formParent.appendChild(model);
    formParent.appendChild(engine);
    formParent.appendChild(topSpeed);
    formParent.appendChild(zeroToSixty);
    formParent.appendChild(mpg);
    formParent.appendChild(price);
    formParent.appendChild(submitButton);
    // getAllMakesForDropDown(function ()
    // {
    //     
    // }

}
// formParent.appendChild(dropDown);



