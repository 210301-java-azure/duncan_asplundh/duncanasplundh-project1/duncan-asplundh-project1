function sendAjaxRequest(method, url, body, successCallback, failureCallback, authToken, username) {
    const xhr = new XMLHttpRequest();
    xhr.open(method, url);
    if (authToken) {
        xhr.setRequestHeader("Authorization", authToken);
        xhr.setRequestHeader("Username", username);
    }
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status > 199 && xhr.status < 300) {
                successCallback(xhr);
            } else {
                failureCallback(xhr);
            }
        }
    }
    if (body) {
        xhr.send(body);
    } else {
        xhr.send();
    }
}

function sendAjaxPost(url, body, successCallback, failureCallback, authToken) {
    sendAjaxRequest("POST", url, body, successCallback, failureCallback, authToken);
}

function ajaxLogin(username, password, successCallback, failureCallback) {
    const payload = `username=${username}&password=${password}`; // "username="+username+"&password"+password
    sendAjaxPost("http://localhost:7000/login", payload, successCallback, failureCallback);
}

