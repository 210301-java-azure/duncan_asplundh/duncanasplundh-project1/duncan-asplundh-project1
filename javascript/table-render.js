function renderSingleCarInTable(car) {
    const tableBody = document.getElementById("table-data-parent");
    let newRow = document.createElement("tr");
    newRow.innerHTML = `<td>${car.makeModel.region}</td><td>${car.makeModel.name}</td><td>${car.name}</td>
        <td>${car.engine}</td><td>${car.topSpeed}mph</td><td>${car.zeroToSixty}s</td><td>${car.mpg}mpg</td>
        <td>$${car.price}</td>`;
    tableBody.appendChild(newRow);
}



function renderItemsInTable(carsList) {
    const tableBody = document.getElementById("table-data-parent");
    for (let car of carsList) {
        let newRow = document.createElement("tr");
        newRow.innerHTML = `<td>${car.makeModel.region}</td><td>${car.makeModel.name}</td><td>${car.name}</td>
        <td>${car.engine}</td><td>${car.topSpeed}mph</td><td>${car.zeroToSixty}s</td><td>${car.mpg}mpg</td>
        <td>$${car.price}</td>`;
        tableBody.appendChild(newRow);
    }
}

function removeAllChildNodes(parent) {
    while (parent.firstChild) {
        parent.removeChild(parent.firstChild);
    }
}

function renderItemsInMakeTable(makes) {
    const tableBody = document.getElementById("table-data-parent");
    for (let make of makes) {
        let newRow = document.createElement("tr");
        newRow.innerHTML = `<td>${make.region}</td><td>${make.name}</td>`;
        tableBody.appendChild(newRow);
    }
}


function renderMakesInDropDown(makes) {
    const formBody = document.getElementById("form-content-parent");
    let dropDown = document.createElement("select");
    formBody.appendChild(dropDown);
    dropDown.id = "make-drop-down";
    for (let make of makes) {
        let newOption = document.createElement("option");
        newOption.id = make.name;
        newOption.value = `{"makeId":"${make.makeId}", "name":"${make.name}", "region":"${make.region}"}`;
        newOption.innerHTML = make.name;
        dropDown.appendChild(newOption)
    }
}

